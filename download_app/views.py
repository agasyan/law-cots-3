import requests
import json
import string

from django.shortcuts import render, redirect
import uuid

# RabbitMQ
import pika
import sys

# Threading
from threading import Thread

# wget
import wget

# CONSTANTS
NPM = '1606918396'
USER = '0806444524'
PASS = '0806444524'
PORT_RABBITMQ = 5672
HOST_RABBITMQ = "152.118.148.95"
VIRTUAL_HOST = '/0806444524'

# Global Var
step_output = 0
routing_id = ""

def index(request):
    return render(request, 'index.html')


def test(request):
    if request.method == "POST":
        try:
            routing_id = str(uuid.uuid4())
            inputfile = request.FILES.get('rawfile')
            # if tidak kebaca
            if inputfile == None:
                msg = 'File not on server'
                status = 'error'
                # if error return to index
                return render(request, 'progress.html', {'status': status, 'msg':msg, 'routing': routing_id, 'response': 'gagal'})
            msg = 'File success read by server 1'
            status = 'ok'
            name = format_filename(inputfile.name)
            # Send Post
            response = requests.post(BACKEND_PRODUCTION, headers={
                'X-ROUTING-KEY': routing_id,
            }, files = {
                'file' : (name, inputfile.read()),
            })
            parsed_json = response.json()

            # print(parsed_json)
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'routing': routing_id, 'response': parsed_json["status"]})
        except Exception as e:
            msg = 'there was an error on file upload server 1, detail: "' + repr(e) +'"'
            # print(msg)
            status = 'error'
            # if error return to index
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'response': 'gagal'})
    else:
        # if method not post return to index
        return redirect(f'/')

def download(request):
    if request.method == "POST":
        try:
            global routing_id
            routing_id = str(uuid.uuid4())
            url_dl = request.POST.get('url-input')
            thread_download_bg(url_dl)
            msg = 'success request file'
            status = 'downloading file'
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'response': 'ok', 'routing': routing_id})
        except Exception as e:
            msg = 'there was an error, detail: "' + repr(e) +'"'
            status = 'error'
            # if error display to html
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'response': 'gagal'})
    else:
        # if method not post return to index
        return redirect(f'/')

def send_mq(msg):
    global routing_id
    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=HOST_RABBITMQ, 
        port=PORT_RABBITMQ,
        virtual_host=VIRTUAL_HOST, 
        credentials=pika.PlainCredentials(USER, PASS)))

    channel = connection.channel()
    channel.exchange_declare(exchange=NPM, exchange_type='direct')
    channel.basic_publish(exchange=NPM, routing_key=routing_id, body=str(msg))
    
def handle_send_progress(percentage):
    global step_output
    if (percentage >= step_output):
        send_mq(step_output)
        step_output += 10
    
def thread_download_bg(url):
    compress_thread = Thread(target = download_file, args = (url, ))
    compress_thread.start()

def download_file(url):
    global step_output
    step_output = 0
    filename = wget.download(url, out="media/download", bar=bar_custom)
    send_mq(filename)

def bar_custom(current, total, width=80):
    handle_send_progress(int(current / total * 100))
